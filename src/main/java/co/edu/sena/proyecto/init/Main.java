package co.edu.sena.proyecto.init;

import co.edu.sena.apiejercicios.jpa.controllers.DepartamentoJpaController;
import co.edu.sena.apiejercicios.jpa.entities.Departamento;
import co.edu.sena.proyecto.apis.ApiDepartamento;
import co.edu.sena.proyecto.apis.ApiMunicipio;
import co.edu.sena.proyecto.apis.ApiRol;
import co.edu.sena.proyecto.utils.JsonTransformer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import spark.Spark;

public class Main {

    public static void main(String[] args) {
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPU_ARCHIVOS");
//        DepartamentoJpaController controller = new DepartamentoJpaController(emf);
//        List<Departamento> dps = controller.findDepartamentoEntities();
//        Departamento d = new Departamento();
//        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
//        String json = gson.toJson(dps);
//        System.out.println(json);

//Ruta de archivos estaticos
        Spark.staticFiles.location("/public");
        //*************
        Spark.port(88);
        //*************
        //*************
        Spark.init();
        //**** Iniciar los servicios
        ApiDepartamento.singleton();
        ApiMunicipio.singleton();
        ApiRol.singleton();
    }

}
