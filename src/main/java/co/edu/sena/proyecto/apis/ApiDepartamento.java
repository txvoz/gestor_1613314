package co.edu.sena.proyecto.apis;

import co.edu.sena.apiejercicios.apis.abstract_.BasicApi;
import co.edu.sena.apiejercicios.apis.abstract_.IApi;
import co.edu.sena.apiejercicios.jpa.controllers.DepartamentoJpaController;
import co.edu.sena.apiejercicios.jpa.entities.Departamento;
import co.edu.sena.proyecto.utils.JsonTransformer;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import spark.Request;
import spark.Response;

public class ApiDepartamento extends BasicApi implements IApi {

    private static ApiDepartamento instance = null;
    private String path = "/departamento";
    private DepartamentoJpaController controller = null;
    private Gson gson = null;

    private ApiDepartamento() {
        init();
        gson = JsonTransformer.singleton().getGson();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPU_ARCHIVOS");
        controller = new DepartamentoJpaController(emf);
    }

    public static ApiDepartamento singleton() {
        if (instance == null) {
            instance = new ApiDepartamento();
        }
        return instance;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public Object create(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        String body = rq.body();
        if (!body.trim().equals("")) {
            try {
                Departamento entity = gson.fromJson(body, Departamento.class);
                controller.create(entity);
                rs.status(201);
                retorno.put("status", 201);
                retorno.put("message", "Creado con exito!");
                retorno.put("data", entity);
            } catch (JsonSyntaxException | PersistenceException e) {
                rs.status(400);
                retorno.put("status", 400);
                retorno.put("message", e.getMessage());
            }
        } else {
            rs.status(400);
            retorno.put("status", 400);
            retorno.put("message", "Error en el body!");
        }
        return retorno;
    }

    @Override
    public Object update(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        try {
            int id = Integer.parseInt(rq.params("id"));
            String body = rq.body();
            Departamento newEntity = gson.fromJson(body, Departamento.class);
            Departamento oldEntity = controller.findDepartamento(id);
            if (oldEntity != null) {
                oldEntity.setDepNombre(newEntity.getDepNombre());
                controller.edit(oldEntity);
                retorno.put("status", 200);
                retorno.put("message", "Registro actualizado con exito!");
                retorno.put("data",oldEntity);
            } else {
                rs.status(404);
                retorno.put("status", 404);
                retorno.put("message", "Registros con id@"+id+" no encontrado!");
            }
        } catch (Exception e) {
            rs.status(400);
            retorno.put("status", 400);
            retorno.put("message", e.getMessage());
        }
        return retorno;
    }

    @Override
    public Object delete(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        try {
            int id = Integer.parseInt(rq.params("id"));
            controller.destroy(id);
            retorno.put("status", 200);
            retorno.put("message", "Eliminado con exito!");
        } catch (Exception e) {
            rs.status(400);
            retorno.put("status", 400);
            retorno.put("message", e.getMessage());
        }
        return retorno;
    }

    @Override
    public Object getAll(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        List<Departamento> departamentos = controller.findDepartamentoEntities();
        if (departamentos.size() > 0) {
            retorno.put("status", 200);
            retorno.put("message", "Registros encontrados");
            retorno.put("data", departamentos);
        } else {
            rs.status(404);
            retorno.put("status", 404);
            retorno.put("message", "No hay registros!");
        }
        return retorno;
    }

    @Override
    public Object getById(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        try {
            int id = Integer.parseInt(rq.params("id"));
            Departamento entity = controller.findDepartamento(id);
            if (entity != null) {
                retorno.put("status", 200);
                retorno.put("message", "Registro encontrado!");
                retorno.put("data", entity);
            } else {
                rs.status(404);
                retorno.put("status", 404);
                retorno.put("message", "Registro con id@" + id + " no encontrado!");
            }
        } catch (Exception e) {
            rs.status(400);
            retorno.put("status", 400);
            retorno.put("message", e.getMessage());
        }
        return retorno;
    }

}
