package co.edu.sena.proyecto.apis;

import co.edu.sena.apiejercicios.apis.abstract_.BasicApi;
import co.edu.sena.apiejercicios.apis.abstract_.IApi;
import co.edu.sena.apiejercicios.jpa.controllers.MunicipioJpaController;
import co.edu.sena.apiejercicios.jpa.entities.Municipio;
import co.edu.sena.proyecto.utils.JsonTransformer;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import spark.Request;
import spark.Response;

public class ApiMunicipio extends BasicApi implements IApi {

    private static ApiMunicipio instance = null;
    private String path = "/municipio";
    private MunicipioJpaController controller = null;
    private Gson gson = null;

    private ApiMunicipio() {
        init();
        gson = JsonTransformer.singleton().getGson();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPU_ARCHIVOS");
        controller = new MunicipioJpaController(emf);
    }

    public static ApiMunicipio singleton() {
        if (instance == null) {
            instance = new ApiMunicipio();
        }
        return instance;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public Object create(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        String body = rq.body();
        if (!body.trim().equals("")) {
            try {
                Municipio entity = gson.fromJson(body, Municipio.class);
                controller.create(entity);
                rs.status(201);
                retorno.put("status", 201);
                retorno.put("message", "Creado con exito!");
                retorno.put("data", entity);
            } catch (JsonSyntaxException | PersistenceException e) {
                rs.status(400);
                retorno.put("status", 400);
                retorno.put("message", e.getMessage());
            }
        } else {
            rs.status(400);
            retorno.put("status", 400);
            retorno.put("message", "Error en el body!");
        }
        return retorno;
    }

    @Override
    public Object update(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        try {
            int id = Integer.parseInt(rq.params("id"));
            String body = rq.body();
            Municipio newEntity = gson.fromJson(body, Municipio.class);
            Municipio oldEntity = controller.findMunicipio(id);
            if (oldEntity != null) {
                oldEntity.setDepId(newEntity.getDepId());
                oldEntity.setMunNombre(newEntity.getMunNombre());
                controller.edit(oldEntity);
                retorno.put("status", 200);
                retorno.put("message", "Registro actualizado con exito!");
                retorno.put("data",oldEntity);
            } else {
                rs.status(404);
                retorno.put("status", 404);
                retorno.put("message", "Registros con id@"+id+" no encontrado!");
            }
        } catch (Exception e) {
            rs.status(400);
            retorno.put("status", 400);
            retorno.put("message", e.getMessage());
        }
        return retorno;
    }

    @Override
    public Object delete(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        try {
            int id = Integer.parseInt(rq.params("id"));
            controller.destroy(id);
            retorno.put("status", 200);
            retorno.put("message", "Eliminado con exito!");
        } catch (Exception e) {
            rs.status(400);
            retorno.put("status", 400);
            retorno.put("message", e.getMessage());
        }
        return retorno;
    }

    @Override
    public Object getAll(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        List<Municipio> departamentos = controller.findMunicipioEntities();
        if (departamentos.size() > 0) {
            retorno.put("status", 200);
            retorno.put("message", "Registros encontrados");
            retorno.put("data", departamentos);
        } else {
            rs.status(404);
            retorno.put("status", 404);
            retorno.put("message", "No hay registros!");
        }
        return retorno;
    }

    @Override
    public Object getById(Request rq, Response rs) {
        Hashtable<String, Object> retorno = new Hashtable<>();
        try {
            int id = Integer.parseInt(rq.params("id"));
            Municipio entity = controller.findMunicipio(id);
            if (entity != null) {
                retorno.put("status", 200);
                retorno.put("message", "Registro encontrado!");
                retorno.put("data", entity);
            } else {
                rs.status(404);
                retorno.put("status", 404);
                retorno.put("message", "Registro con id@" + id + " no encontrado!");
            }
        } catch (Exception e) {
            rs.status(400);
            retorno.put("status", 400);
            retorno.put("message", e.getMessage());
        }
        return retorno;
    }

}
