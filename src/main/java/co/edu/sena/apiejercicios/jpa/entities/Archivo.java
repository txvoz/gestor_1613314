/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "archivo", catalog = "db_gestion_archivos", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Archivo.findAll", query = "SELECT a FROM Archivo a")
    , @NamedQuery(name = "Archivo.findByArcId", query = "SELECT a FROM Archivo a WHERE a.arcId = :arcId")
    , @NamedQuery(name = "Archivo.findByArcNombre", query = "SELECT a FROM Archivo a WHERE a.arcNombre = :arcNombre")
    , @NamedQuery(name = "Archivo.findByArcPeso", query = "SELECT a FROM Archivo a WHERE a.arcPeso = :arcPeso")
    , @NamedQuery(name = "Archivo.findByArcFechaCreacion", query = "SELECT a FROM Archivo a WHERE a.arcFechaCreacion = :arcFechaCreacion")
    , @NamedQuery(name = "Archivo.findByArcEstado", query = "SELECT a FROM Archivo a WHERE a.arcEstado = :arcEstado")
    , @NamedQuery(name = "Archivo.findByArcPath", query = "SELECT a FROM Archivo a WHERE a.arcPath = :arcPath")})
public class Archivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "arcId", nullable = false)
    private Integer arcId;
    @Basic(optional = false)
    @Column(name = "arcNombre", nullable = false, length = 45)
    private String arcNombre;
    @Basic(optional = false)
    @Column(name = "arcPeso", nullable = false, length = 20)
    private String arcPeso;
    @Basic(optional = false)
    @Column(name = "arcFechaCreacion", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date arcFechaCreacion;
    @Basic(optional = false)
    @Lob
    @Column(name = "arcPalabrasClave", nullable = false, length = 65535)
    private String arcPalabrasClave;
    @Basic(optional = false)
    @Column(name = "arcEstado", nullable = false, length = 8)
    private String arcEstado;
    @Basic(optional = false)
    @Column(name = "arcPath", nullable = false, length = 60)
    private String arcPath;
    @JoinColumn(name = "catId", referencedColumnName = "catId", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private CategoriaArchivo catId;
    @JoinColumn(name = "tpaId", referencedColumnName = "tpaId", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private TipoArchivo tpaId;
    @JoinColumn(name = "usuId", referencedColumnName = "usuId", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario usuId;

    public Archivo() {
    }

    public Archivo(Integer arcId) {
        this.arcId = arcId;
    }

    public Archivo(Integer arcId, String arcNombre, String arcPeso, Date arcFechaCreacion, String arcPalabrasClave, String arcEstado, String arcPath) {
        this.arcId = arcId;
        this.arcNombre = arcNombre;
        this.arcPeso = arcPeso;
        this.arcFechaCreacion = arcFechaCreacion;
        this.arcPalabrasClave = arcPalabrasClave;
        this.arcEstado = arcEstado;
        this.arcPath = arcPath;
    }

    public Integer getArcId() {
        return arcId;
    }

    public void setArcId(Integer arcId) {
        this.arcId = arcId;
    }

    public String getArcNombre() {
        return arcNombre;
    }

    public void setArcNombre(String arcNombre) {
        this.arcNombre = arcNombre;
    }

    public String getArcPeso() {
        return arcPeso;
    }

    public void setArcPeso(String arcPeso) {
        this.arcPeso = arcPeso;
    }

    public Date getArcFechaCreacion() {
        return arcFechaCreacion;
    }

    public void setArcFechaCreacion(Date arcFechaCreacion) {
        this.arcFechaCreacion = arcFechaCreacion;
    }

    public String getArcPalabrasClave() {
        return arcPalabrasClave;
    }

    public void setArcPalabrasClave(String arcPalabrasClave) {
        this.arcPalabrasClave = arcPalabrasClave;
    }

    public String getArcEstado() {
        return arcEstado;
    }

    public void setArcEstado(String arcEstado) {
        this.arcEstado = arcEstado;
    }

    public String getArcPath() {
        return arcPath;
    }

    public void setArcPath(String arcPath) {
        this.arcPath = arcPath;
    }

    public CategoriaArchivo getCatId() {
        return catId;
    }

    public void setCatId(CategoriaArchivo catId) {
        this.catId = catId;
    }

    public TipoArchivo getTpaId() {
        return tpaId;
    }

    public void setTpaId(TipoArchivo tpaId) {
        this.tpaId = tpaId;
    }

    public Usuario getUsuId() {
        return usuId;
    }

    public void setUsuId(Usuario usuId) {
        this.usuId = usuId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (arcId != null ? arcId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Archivo)) {
            return false;
        }
        Archivo other = (Archivo) object;
        if ((this.arcId == null && other.arcId != null) || (this.arcId != null && !this.arcId.equals(other.arcId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.sena.apiejercicios.jpa.entities.Archivo[ arcId=" + arcId + " ]";
    }
    
}
