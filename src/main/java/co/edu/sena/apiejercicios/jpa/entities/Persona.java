/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "persona", catalog = "db_gestion_archivos", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"perIdentificacion"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p")
    , @NamedQuery(name = "Persona.findByPerId", query = "SELECT p FROM Persona p WHERE p.perId = :perId")
    , @NamedQuery(name = "Persona.findByPerNombre", query = "SELECT p FROM Persona p WHERE p.perNombre = :perNombre")
    , @NamedQuery(name = "Persona.findByPerApellido", query = "SELECT p FROM Persona p WHERE p.perApellido = :perApellido")
    , @NamedQuery(name = "Persona.findByPerIdentificacion", query = "SELECT p FROM Persona p WHERE p.perIdentificacion = :perIdentificacion")
    , @NamedQuery(name = "Persona.findByPerFechaNacimiento", query = "SELECT p FROM Persona p WHERE p.perFechaNacimiento = :perFechaNacimiento")
    , @NamedQuery(name = "Persona.findByPerGenero", query = "SELECT p FROM Persona p WHERE p.perGenero = :perGenero")})
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "perId", nullable = false)
    private Integer perId;
    @Basic(optional = false)
    @Column(name = "perNombre", nullable = false, length = 45)
    private String perNombre;
    @Basic(optional = false)
    @Column(name = "perApellido", nullable = false, length = 45)
    private String perApellido;
    @Basic(optional = false)
    @Column(name = "perIdentificacion", nullable = false, length = 45)
    private String perIdentificacion;
    @Basic(optional = false)
    @Column(name = "perFechaNacimiento", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date perFechaNacimiento;
    @Basic(optional = false)
    @Column(name = "perGenero", nullable = false, length = 2)
    private String perGenero;
    @JoinColumn(name = "munId_nacimiento", referencedColumnName = "munId", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Municipio munIdnacimiento;
    @JoinColumn(name = "munId_residencia", referencedColumnName = "munId", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Municipio munIdresidencia;
    @JoinColumn(name = "tipId", referencedColumnName = "tipId", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private TipoIdentificacion tipId;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "perId", fetch = FetchType.EAGER)
    private Usuario usuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "perId", fetch = FetchType.EAGER)
    private List<Telefono> telefonoList;

    public Persona() {
    }

    public Persona(Integer perId) {
        this.perId = perId;
    }

    public Persona(Integer perId, String perNombre, String perApellido, String perIdentificacion, Date perFechaNacimiento, String perGenero) {
        this.perId = perId;
        this.perNombre = perNombre;
        this.perApellido = perApellido;
        this.perIdentificacion = perIdentificacion;
        this.perFechaNacimiento = perFechaNacimiento;
        this.perGenero = perGenero;
    }

    public Integer getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    public String getPerNombre() {
        return perNombre;
    }

    public void setPerNombre(String perNombre) {
        this.perNombre = perNombre;
    }

    public String getPerApellido() {
        return perApellido;
    }

    public void setPerApellido(String perApellido) {
        this.perApellido = perApellido;
    }

    public String getPerIdentificacion() {
        return perIdentificacion;
    }

    public void setPerIdentificacion(String perIdentificacion) {
        this.perIdentificacion = perIdentificacion;
    }

    public Date getPerFechaNacimiento() {
        return perFechaNacimiento;
    }

    public void setPerFechaNacimiento(Date perFechaNacimiento) {
        this.perFechaNacimiento = perFechaNacimiento;
    }

    public String getPerGenero() {
        return perGenero;
    }

    public void setPerGenero(String perGenero) {
        this.perGenero = perGenero;
    }

    public Municipio getMunIdnacimiento() {
        return munIdnacimiento;
    }

    public void setMunIdnacimiento(Municipio munIdnacimiento) {
        this.munIdnacimiento = munIdnacimiento;
    }

    public Municipio getMunIdresidencia() {
        return munIdresidencia;
    }

    public void setMunIdresidencia(Municipio munIdresidencia) {
        this.munIdresidencia = munIdresidencia;
    }

    public TipoIdentificacion getTipId() {
        return tipId;
    }

    public void setTipId(TipoIdentificacion tipId) {
        this.tipId = tipId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @XmlTransient
    public List<Telefono> getTelefonoList() {
        return telefonoList;
    }

    public void setTelefonoList(List<Telefono> telefonoList) {
        this.telefonoList = telefonoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perId != null ? perId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.perId == null && other.perId != null) || (this.perId != null && !this.perId.equals(other.perId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.sena.apiejercicios.jpa.entities.Persona[ perId=" + perId + " ]";
    }
    
}
