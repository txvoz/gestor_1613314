/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.controllers;

import co.edu.sena.apiejercicios.jpa.controllers.exceptions.IllegalOrphanException;
import co.edu.sena.apiejercicios.jpa.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.edu.sena.apiejercicios.jpa.entities.Persona;
import co.edu.sena.apiejercicios.jpa.entities.Rol;
import co.edu.sena.apiejercicios.jpa.entities.Archivo;
import co.edu.sena.apiejercicios.jpa.entities.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author USER
 */
public class UsuarioJpaController implements Serializable {

    public UsuarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) throws IllegalOrphanException {
        if (usuario.getArchivoList() == null) {
            usuario.setArchivoList(new ArrayList<Archivo>());
        }
        List<String> illegalOrphanMessages = null;
        Persona perIdOrphanCheck = usuario.getPerId();
        if (perIdOrphanCheck != null) {
            Usuario oldUsuarioOfPerId = perIdOrphanCheck.getUsuario();
            if (oldUsuarioOfPerId != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("The Persona " + perIdOrphanCheck + " already has an item of type Usuario whose perId column cannot be null. Please make another selection for the perId field.");
            }
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Persona perId = usuario.getPerId();
            if (perId != null) {
                perId = em.getReference(perId.getClass(), perId.getPerId());
                usuario.setPerId(perId);
            }
            Rol rolId = usuario.getRolId();
            if (rolId != null) {
                rolId = em.getReference(rolId.getClass(), rolId.getRolId());
                usuario.setRolId(rolId);
            }
            List<Archivo> attachedArchivoList = new ArrayList<Archivo>();
            for (Archivo archivoListArchivoToAttach : usuario.getArchivoList()) {
                archivoListArchivoToAttach = em.getReference(archivoListArchivoToAttach.getClass(), archivoListArchivoToAttach.getArcId());
                attachedArchivoList.add(archivoListArchivoToAttach);
            }
            usuario.setArchivoList(attachedArchivoList);
            em.persist(usuario);
            if (perId != null) {
                perId.setUsuario(usuario);
                perId = em.merge(perId);
            }
            if (rolId != null) {
                rolId.getUsuarioList().add(usuario);
                rolId = em.merge(rolId);
            }
            for (Archivo archivoListArchivo : usuario.getArchivoList()) {
                Usuario oldUsuIdOfArchivoListArchivo = archivoListArchivo.getUsuId();
                archivoListArchivo.setUsuId(usuario);
                archivoListArchivo = em.merge(archivoListArchivo);
                if (oldUsuIdOfArchivoListArchivo != null) {
                    oldUsuIdOfArchivoListArchivo.getArchivoList().remove(archivoListArchivo);
                    oldUsuIdOfArchivoListArchivo = em.merge(oldUsuIdOfArchivoListArchivo);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuario usuario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario persistentUsuario = em.find(Usuario.class, usuario.getUsuId());
            Persona perIdOld = persistentUsuario.getPerId();
            Persona perIdNew = usuario.getPerId();
            Rol rolIdOld = persistentUsuario.getRolId();
            Rol rolIdNew = usuario.getRolId();
            List<Archivo> archivoListOld = persistentUsuario.getArchivoList();
            List<Archivo> archivoListNew = usuario.getArchivoList();
            List<String> illegalOrphanMessages = null;
            if (perIdNew != null && !perIdNew.equals(perIdOld)) {
                Usuario oldUsuarioOfPerId = perIdNew.getUsuario();
                if (oldUsuarioOfPerId != null) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("The Persona " + perIdNew + " already has an item of type Usuario whose perId column cannot be null. Please make another selection for the perId field.");
                }
            }
            for (Archivo archivoListOldArchivo : archivoListOld) {
                if (!archivoListNew.contains(archivoListOldArchivo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Archivo " + archivoListOldArchivo + " since its usuId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (perIdNew != null) {
                perIdNew = em.getReference(perIdNew.getClass(), perIdNew.getPerId());
                usuario.setPerId(perIdNew);
            }
            if (rolIdNew != null) {
                rolIdNew = em.getReference(rolIdNew.getClass(), rolIdNew.getRolId());
                usuario.setRolId(rolIdNew);
            }
            List<Archivo> attachedArchivoListNew = new ArrayList<Archivo>();
            for (Archivo archivoListNewArchivoToAttach : archivoListNew) {
                archivoListNewArchivoToAttach = em.getReference(archivoListNewArchivoToAttach.getClass(), archivoListNewArchivoToAttach.getArcId());
                attachedArchivoListNew.add(archivoListNewArchivoToAttach);
            }
            archivoListNew = attachedArchivoListNew;
            usuario.setArchivoList(archivoListNew);
            usuario = em.merge(usuario);
            if (perIdOld != null && !perIdOld.equals(perIdNew)) {
                perIdOld.setUsuario(null);
                perIdOld = em.merge(perIdOld);
            }
            if (perIdNew != null && !perIdNew.equals(perIdOld)) {
                perIdNew.setUsuario(usuario);
                perIdNew = em.merge(perIdNew);
            }
            if (rolIdOld != null && !rolIdOld.equals(rolIdNew)) {
                rolIdOld.getUsuarioList().remove(usuario);
                rolIdOld = em.merge(rolIdOld);
            }
            if (rolIdNew != null && !rolIdNew.equals(rolIdOld)) {
                rolIdNew.getUsuarioList().add(usuario);
                rolIdNew = em.merge(rolIdNew);
            }
            for (Archivo archivoListNewArchivo : archivoListNew) {
                if (!archivoListOld.contains(archivoListNewArchivo)) {
                    Usuario oldUsuIdOfArchivoListNewArchivo = archivoListNewArchivo.getUsuId();
                    archivoListNewArchivo.setUsuId(usuario);
                    archivoListNewArchivo = em.merge(archivoListNewArchivo);
                    if (oldUsuIdOfArchivoListNewArchivo != null && !oldUsuIdOfArchivoListNewArchivo.equals(usuario)) {
                        oldUsuIdOfArchivoListNewArchivo.getArchivoList().remove(archivoListNewArchivo);
                        oldUsuIdOfArchivoListNewArchivo = em.merge(oldUsuIdOfArchivoListNewArchivo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = usuario.getUsuId();
                if (findUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario;
            try {
                usuario = em.getReference(Usuario.class, id);
                usuario.getUsuId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Archivo> archivoListOrphanCheck = usuario.getArchivoList();
            for (Archivo archivoListOrphanCheckArchivo : archivoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Archivo " + archivoListOrphanCheckArchivo + " in its archivoList field has a non-nullable usuId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Persona perId = usuario.getPerId();
            if (perId != null) {
                perId.setUsuario(null);
                perId = em.merge(perId);
            }
            Rol rolId = usuario.getRolId();
            if (rolId != null) {
                rolId.getUsuarioList().remove(usuario);
                rolId = em.merge(rolId);
            }
            em.remove(usuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuario> findUsuarioEntities() {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult) {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findUsuario(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuario> rt = cq.from(Usuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
