/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "tipo_archivo", catalog = "db_gestion_archivos", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"tpaExt"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoArchivo.findAll", query = "SELECT t FROM TipoArchivo t")
    , @NamedQuery(name = "TipoArchivo.findByTpaId", query = "SELECT t FROM TipoArchivo t WHERE t.tpaId = :tpaId")
    , @NamedQuery(name = "TipoArchivo.findByTpaNombre", query = "SELECT t FROM TipoArchivo t WHERE t.tpaNombre = :tpaNombre")
    , @NamedQuery(name = "TipoArchivo.findByTpaExt", query = "SELECT t FROM TipoArchivo t WHERE t.tpaExt = :tpaExt")})
public class TipoArchivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tpaId", nullable = false)
    private Integer tpaId;
    @Basic(optional = false)
    @Column(name = "tpaNombre", nullable = false, length = 30)
    private String tpaNombre;
    @Basic(optional = false)
    @Column(name = "tpaExt", nullable = false, length = 5)
    private String tpaExt;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tpaId", fetch = FetchType.EAGER)
    private List<Archivo> archivoList;

    public TipoArchivo() {
    }

    public TipoArchivo(Integer tpaId) {
        this.tpaId = tpaId;
    }

    public TipoArchivo(Integer tpaId, String tpaNombre, String tpaExt) {
        this.tpaId = tpaId;
        this.tpaNombre = tpaNombre;
        this.tpaExt = tpaExt;
    }

    public Integer getTpaId() {
        return tpaId;
    }

    public void setTpaId(Integer tpaId) {
        this.tpaId = tpaId;
    }

    public String getTpaNombre() {
        return tpaNombre;
    }

    public void setTpaNombre(String tpaNombre) {
        this.tpaNombre = tpaNombre;
    }

    public String getTpaExt() {
        return tpaExt;
    }

    public void setTpaExt(String tpaExt) {
        this.tpaExt = tpaExt;
    }

    @XmlTransient
    public List<Archivo> getArchivoList() {
        return archivoList;
    }

    public void setArchivoList(List<Archivo> archivoList) {
        this.archivoList = archivoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tpaId != null ? tpaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoArchivo)) {
            return false;
        }
        TipoArchivo other = (TipoArchivo) object;
        if ((this.tpaId == null && other.tpaId != null) || (this.tpaId != null && !this.tpaId.equals(other.tpaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.sena.apiejercicios.jpa.entities.TipoArchivo[ tpaId=" + tpaId + " ]";
    }
    
}
