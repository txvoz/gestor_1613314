/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.controllers;

import co.edu.sena.apiejercicios.jpa.controllers.exceptions.IllegalOrphanException;
import co.edu.sena.apiejercicios.jpa.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.edu.sena.apiejercicios.jpa.entities.Archivo;
import co.edu.sena.apiejercicios.jpa.entities.TipoArchivo;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author USER
 */
public class TipoArchivoJpaController implements Serializable {

    public TipoArchivoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoArchivo tipoArchivo) {
        if (tipoArchivo.getArchivoList() == null) {
            tipoArchivo.setArchivoList(new ArrayList<Archivo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Archivo> attachedArchivoList = new ArrayList<Archivo>();
            for (Archivo archivoListArchivoToAttach : tipoArchivo.getArchivoList()) {
                archivoListArchivoToAttach = em.getReference(archivoListArchivoToAttach.getClass(), archivoListArchivoToAttach.getArcId());
                attachedArchivoList.add(archivoListArchivoToAttach);
            }
            tipoArchivo.setArchivoList(attachedArchivoList);
            em.persist(tipoArchivo);
            for (Archivo archivoListArchivo : tipoArchivo.getArchivoList()) {
                TipoArchivo oldTpaIdOfArchivoListArchivo = archivoListArchivo.getTpaId();
                archivoListArchivo.setTpaId(tipoArchivo);
                archivoListArchivo = em.merge(archivoListArchivo);
                if (oldTpaIdOfArchivoListArchivo != null) {
                    oldTpaIdOfArchivoListArchivo.getArchivoList().remove(archivoListArchivo);
                    oldTpaIdOfArchivoListArchivo = em.merge(oldTpaIdOfArchivoListArchivo);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoArchivo tipoArchivo) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoArchivo persistentTipoArchivo = em.find(TipoArchivo.class, tipoArchivo.getTpaId());
            List<Archivo> archivoListOld = persistentTipoArchivo.getArchivoList();
            List<Archivo> archivoListNew = tipoArchivo.getArchivoList();
            List<String> illegalOrphanMessages = null;
            for (Archivo archivoListOldArchivo : archivoListOld) {
                if (!archivoListNew.contains(archivoListOldArchivo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Archivo " + archivoListOldArchivo + " since its tpaId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Archivo> attachedArchivoListNew = new ArrayList<Archivo>();
            for (Archivo archivoListNewArchivoToAttach : archivoListNew) {
                archivoListNewArchivoToAttach = em.getReference(archivoListNewArchivoToAttach.getClass(), archivoListNewArchivoToAttach.getArcId());
                attachedArchivoListNew.add(archivoListNewArchivoToAttach);
            }
            archivoListNew = attachedArchivoListNew;
            tipoArchivo.setArchivoList(archivoListNew);
            tipoArchivo = em.merge(tipoArchivo);
            for (Archivo archivoListNewArchivo : archivoListNew) {
                if (!archivoListOld.contains(archivoListNewArchivo)) {
                    TipoArchivo oldTpaIdOfArchivoListNewArchivo = archivoListNewArchivo.getTpaId();
                    archivoListNewArchivo.setTpaId(tipoArchivo);
                    archivoListNewArchivo = em.merge(archivoListNewArchivo);
                    if (oldTpaIdOfArchivoListNewArchivo != null && !oldTpaIdOfArchivoListNewArchivo.equals(tipoArchivo)) {
                        oldTpaIdOfArchivoListNewArchivo.getArchivoList().remove(archivoListNewArchivo);
                        oldTpaIdOfArchivoListNewArchivo = em.merge(oldTpaIdOfArchivoListNewArchivo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoArchivo.getTpaId();
                if (findTipoArchivo(id) == null) {
                    throw new NonexistentEntityException("The tipoArchivo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoArchivo tipoArchivo;
            try {
                tipoArchivo = em.getReference(TipoArchivo.class, id);
                tipoArchivo.getTpaId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoArchivo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Archivo> archivoListOrphanCheck = tipoArchivo.getArchivoList();
            for (Archivo archivoListOrphanCheckArchivo : archivoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TipoArchivo (" + tipoArchivo + ") cannot be destroyed since the Archivo " + archivoListOrphanCheckArchivo + " in its archivoList field has a non-nullable tpaId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(tipoArchivo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoArchivo> findTipoArchivoEntities() {
        return findTipoArchivoEntities(true, -1, -1);
    }

    public List<TipoArchivo> findTipoArchivoEntities(int maxResults, int firstResult) {
        return findTipoArchivoEntities(false, maxResults, firstResult);
    }

    private List<TipoArchivo> findTipoArchivoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoArchivo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoArchivo findTipoArchivo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoArchivo.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoArchivoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoArchivo> rt = cq.from(TipoArchivo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
