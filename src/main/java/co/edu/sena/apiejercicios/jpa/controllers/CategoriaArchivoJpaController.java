/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.controllers;

import co.edu.sena.apiejercicios.jpa.controllers.exceptions.IllegalOrphanException;
import co.edu.sena.apiejercicios.jpa.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.edu.sena.apiejercicios.jpa.entities.Archivo;
import co.edu.sena.apiejercicios.jpa.entities.CategoriaArchivo;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author USER
 */
public class CategoriaArchivoJpaController implements Serializable {

    public CategoriaArchivoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CategoriaArchivo categoriaArchivo) {
        if (categoriaArchivo.getArchivoList() == null) {
            categoriaArchivo.setArchivoList(new ArrayList<Archivo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Archivo> attachedArchivoList = new ArrayList<Archivo>();
            for (Archivo archivoListArchivoToAttach : categoriaArchivo.getArchivoList()) {
                archivoListArchivoToAttach = em.getReference(archivoListArchivoToAttach.getClass(), archivoListArchivoToAttach.getArcId());
                attachedArchivoList.add(archivoListArchivoToAttach);
            }
            categoriaArchivo.setArchivoList(attachedArchivoList);
            em.persist(categoriaArchivo);
            for (Archivo archivoListArchivo : categoriaArchivo.getArchivoList()) {
                CategoriaArchivo oldCatIdOfArchivoListArchivo = archivoListArchivo.getCatId();
                archivoListArchivo.setCatId(categoriaArchivo);
                archivoListArchivo = em.merge(archivoListArchivo);
                if (oldCatIdOfArchivoListArchivo != null) {
                    oldCatIdOfArchivoListArchivo.getArchivoList().remove(archivoListArchivo);
                    oldCatIdOfArchivoListArchivo = em.merge(oldCatIdOfArchivoListArchivo);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CategoriaArchivo categoriaArchivo) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CategoriaArchivo persistentCategoriaArchivo = em.find(CategoriaArchivo.class, categoriaArchivo.getCatId());
            List<Archivo> archivoListOld = persistentCategoriaArchivo.getArchivoList();
            List<Archivo> archivoListNew = categoriaArchivo.getArchivoList();
            List<String> illegalOrphanMessages = null;
            for (Archivo archivoListOldArchivo : archivoListOld) {
                if (!archivoListNew.contains(archivoListOldArchivo)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Archivo " + archivoListOldArchivo + " since its catId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Archivo> attachedArchivoListNew = new ArrayList<Archivo>();
            for (Archivo archivoListNewArchivoToAttach : archivoListNew) {
                archivoListNewArchivoToAttach = em.getReference(archivoListNewArchivoToAttach.getClass(), archivoListNewArchivoToAttach.getArcId());
                attachedArchivoListNew.add(archivoListNewArchivoToAttach);
            }
            archivoListNew = attachedArchivoListNew;
            categoriaArchivo.setArchivoList(archivoListNew);
            categoriaArchivo = em.merge(categoriaArchivo);
            for (Archivo archivoListNewArchivo : archivoListNew) {
                if (!archivoListOld.contains(archivoListNewArchivo)) {
                    CategoriaArchivo oldCatIdOfArchivoListNewArchivo = archivoListNewArchivo.getCatId();
                    archivoListNewArchivo.setCatId(categoriaArchivo);
                    archivoListNewArchivo = em.merge(archivoListNewArchivo);
                    if (oldCatIdOfArchivoListNewArchivo != null && !oldCatIdOfArchivoListNewArchivo.equals(categoriaArchivo)) {
                        oldCatIdOfArchivoListNewArchivo.getArchivoList().remove(archivoListNewArchivo);
                        oldCatIdOfArchivoListNewArchivo = em.merge(oldCatIdOfArchivoListNewArchivo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = categoriaArchivo.getCatId();
                if (findCategoriaArchivo(id) == null) {
                    throw new NonexistentEntityException("The categoriaArchivo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CategoriaArchivo categoriaArchivo;
            try {
                categoriaArchivo = em.getReference(CategoriaArchivo.class, id);
                categoriaArchivo.getCatId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The categoriaArchivo with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Archivo> archivoListOrphanCheck = categoriaArchivo.getArchivoList();
            for (Archivo archivoListOrphanCheckArchivo : archivoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CategoriaArchivo (" + categoriaArchivo + ") cannot be destroyed since the Archivo " + archivoListOrphanCheckArchivo + " in its archivoList field has a non-nullable catId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(categoriaArchivo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CategoriaArchivo> findCategoriaArchivoEntities() {
        return findCategoriaArchivoEntities(true, -1, -1);
    }

    public List<CategoriaArchivo> findCategoriaArchivoEntities(int maxResults, int firstResult) {
        return findCategoriaArchivoEntities(false, maxResults, firstResult);
    }

    private List<CategoriaArchivo> findCategoriaArchivoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CategoriaArchivo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CategoriaArchivo findCategoriaArchivo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CategoriaArchivo.class, id);
        } finally {
            em.close();
        }
    }

    public int getCategoriaArchivoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CategoriaArchivo> rt = cq.from(CategoriaArchivo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
