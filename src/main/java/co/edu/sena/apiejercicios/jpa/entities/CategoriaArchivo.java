/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "categoria_archivo", catalog = "db_gestion_archivos", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"catNombre"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriaArchivo.findAll", query = "SELECT c FROM CategoriaArchivo c")
    , @NamedQuery(name = "CategoriaArchivo.findByCatId", query = "SELECT c FROM CategoriaArchivo c WHERE c.catId = :catId")
    , @NamedQuery(name = "CategoriaArchivo.findByCatNombre", query = "SELECT c FROM CategoriaArchivo c WHERE c.catNombre = :catNombre")})
public class CategoriaArchivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "catId", nullable = false)
    private Integer catId;
    @Basic(optional = false)
    @Column(name = "catNombre", nullable = false, length = 45)
    private String catNombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catId", fetch = FetchType.EAGER)
    private List<Archivo> archivoList;

    public CategoriaArchivo() {
    }

    public CategoriaArchivo(Integer catId) {
        this.catId = catId;
    }

    public CategoriaArchivo(Integer catId, String catNombre) {
        this.catId = catId;
        this.catNombre = catNombre;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getCatNombre() {
        return catNombre;
    }

    public void setCatNombre(String catNombre) {
        this.catNombre = catNombre;
    }

    @XmlTransient
    public List<Archivo> getArchivoList() {
        return archivoList;
    }

    public void setArchivoList(List<Archivo> archivoList) {
        this.archivoList = archivoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catId != null ? catId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaArchivo)) {
            return false;
        }
        CategoriaArchivo other = (CategoriaArchivo) object;
        if ((this.catId == null && other.catId != null) || (this.catId != null && !this.catId.equals(other.catId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.sena.apiejercicios.jpa.entities.CategoriaArchivo[ catId=" + catId + " ]";
    }
    
}
