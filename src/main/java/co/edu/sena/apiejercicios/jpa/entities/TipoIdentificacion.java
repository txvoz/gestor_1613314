/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "tipo_identificacion", catalog = "db_gestion_archivos", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"tipNombre"})
    , @UniqueConstraint(columnNames = {"tipNomenclatura"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoIdentificacion.findAll", query = "SELECT t FROM TipoIdentificacion t")
    , @NamedQuery(name = "TipoIdentificacion.findByTipId", query = "SELECT t FROM TipoIdentificacion t WHERE t.tipId = :tipId")
    , @NamedQuery(name = "TipoIdentificacion.findByTipNombre", query = "SELECT t FROM TipoIdentificacion t WHERE t.tipNombre = :tipNombre")
    , @NamedQuery(name = "TipoIdentificacion.findByTipNomenclatura", query = "SELECT t FROM TipoIdentificacion t WHERE t.tipNomenclatura = :tipNomenclatura")})
public class TipoIdentificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tipId", nullable = false)
    private Integer tipId;
    @Basic(optional = false)
    @Column(name = "tipNombre", nullable = false, length = 50)
    private String tipNombre;
    @Basic(optional = false)
    @Column(name = "tipNomenclatura", nullable = false, length = 10)
    private String tipNomenclatura;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipId", fetch = FetchType.EAGER)
    private List<Persona> personaList;

    public TipoIdentificacion() {
    }

    public TipoIdentificacion(Integer tipId) {
        this.tipId = tipId;
    }

    public TipoIdentificacion(Integer tipId, String tipNombre, String tipNomenclatura) {
        this.tipId = tipId;
        this.tipNombre = tipNombre;
        this.tipNomenclatura = tipNomenclatura;
    }

    public Integer getTipId() {
        return tipId;
    }

    public void setTipId(Integer tipId) {
        this.tipId = tipId;
    }

    public String getTipNombre() {
        return tipNombre;
    }

    public void setTipNombre(String tipNombre) {
        this.tipNombre = tipNombre;
    }

    public String getTipNomenclatura() {
        return tipNomenclatura;
    }

    public void setTipNomenclatura(String tipNomenclatura) {
        this.tipNomenclatura = tipNomenclatura;
    }

    @XmlTransient
    public List<Persona> getPersonaList() {
        return personaList;
    }

    public void setPersonaList(List<Persona> personaList) {
        this.personaList = personaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipId != null ? tipId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoIdentificacion)) {
            return false;
        }
        TipoIdentificacion other = (TipoIdentificacion) object;
        if ((this.tipId == null && other.tipId != null) || (this.tipId != null && !this.tipId.equals(other.tipId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.sena.apiejercicios.jpa.entities.TipoIdentificacion[ tipId=" + tipId + " ]";
    }
    
}
