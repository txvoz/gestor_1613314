/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.controllers;

import co.edu.sena.apiejercicios.jpa.controllers.exceptions.IllegalOrphanException;
import co.edu.sena.apiejercicios.jpa.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.edu.sena.apiejercicios.jpa.entities.Municipio;
import co.edu.sena.apiejercicios.jpa.entities.Persona;
import co.edu.sena.apiejercicios.jpa.entities.TipoIdentificacion;
import co.edu.sena.apiejercicios.jpa.entities.Usuario;
import co.edu.sena.apiejercicios.jpa.entities.Telefono;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author USER
 */
public class PersonaJpaController implements Serializable {

    public PersonaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Persona persona) {
        if (persona.getTelefonoList() == null) {
            persona.setTelefonoList(new ArrayList<Telefono>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio munIdnacimiento = persona.getMunIdnacimiento();
            if (munIdnacimiento != null) {
                munIdnacimiento = em.getReference(munIdnacimiento.getClass(), munIdnacimiento.getMunId());
                persona.setMunIdnacimiento(munIdnacimiento);
            }
            Municipio munIdresidencia = persona.getMunIdresidencia();
            if (munIdresidencia != null) {
                munIdresidencia = em.getReference(munIdresidencia.getClass(), munIdresidencia.getMunId());
                persona.setMunIdresidencia(munIdresidencia);
            }
            TipoIdentificacion tipId = persona.getTipId();
            if (tipId != null) {
                tipId = em.getReference(tipId.getClass(), tipId.getTipId());
                persona.setTipId(tipId);
            }
            Usuario usuario = persona.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getUsuId());
                persona.setUsuario(usuario);
            }
            List<Telefono> attachedTelefonoList = new ArrayList<Telefono>();
            for (Telefono telefonoListTelefonoToAttach : persona.getTelefonoList()) {
                telefonoListTelefonoToAttach = em.getReference(telefonoListTelefonoToAttach.getClass(), telefonoListTelefonoToAttach.getTelId());
                attachedTelefonoList.add(telefonoListTelefonoToAttach);
            }
            persona.setTelefonoList(attachedTelefonoList);
            em.persist(persona);
            if (munIdnacimiento != null) {
                munIdnacimiento.getPersonaList().add(persona);
                munIdnacimiento = em.merge(munIdnacimiento);
            }
            if (munIdresidencia != null) {
                munIdresidencia.getPersonaList().add(persona);
                munIdresidencia = em.merge(munIdresidencia);
            }
            if (tipId != null) {
                tipId.getPersonaList().add(persona);
                tipId = em.merge(tipId);
            }
            if (usuario != null) {
                Persona oldPerIdOfUsuario = usuario.getPerId();
                if (oldPerIdOfUsuario != null) {
                    oldPerIdOfUsuario.setUsuario(null);
                    oldPerIdOfUsuario = em.merge(oldPerIdOfUsuario);
                }
                usuario.setPerId(persona);
                usuario = em.merge(usuario);
            }
            for (Telefono telefonoListTelefono : persona.getTelefonoList()) {
                Persona oldPerIdOfTelefonoListTelefono = telefonoListTelefono.getPerId();
                telefonoListTelefono.setPerId(persona);
                telefonoListTelefono = em.merge(telefonoListTelefono);
                if (oldPerIdOfTelefonoListTelefono != null) {
                    oldPerIdOfTelefonoListTelefono.getTelefonoList().remove(telefonoListTelefono);
                    oldPerIdOfTelefonoListTelefono = em.merge(oldPerIdOfTelefonoListTelefono);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Persona persona) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Persona persistentPersona = em.find(Persona.class, persona.getPerId());
            Municipio munIdnacimientoOld = persistentPersona.getMunIdnacimiento();
            Municipio munIdnacimientoNew = persona.getMunIdnacimiento();
            Municipio munIdresidenciaOld = persistentPersona.getMunIdresidencia();
            Municipio munIdresidenciaNew = persona.getMunIdresidencia();
            TipoIdentificacion tipIdOld = persistentPersona.getTipId();
            TipoIdentificacion tipIdNew = persona.getTipId();
            Usuario usuarioOld = persistentPersona.getUsuario();
            Usuario usuarioNew = persona.getUsuario();
            List<Telefono> telefonoListOld = persistentPersona.getTelefonoList();
            List<Telefono> telefonoListNew = persona.getTelefonoList();
            List<String> illegalOrphanMessages = null;
            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain Usuario " + usuarioOld + " since its perId field is not nullable.");
            }
            for (Telefono telefonoListOldTelefono : telefonoListOld) {
                if (!telefonoListNew.contains(telefonoListOldTelefono)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Telefono " + telefonoListOldTelefono + " since its perId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (munIdnacimientoNew != null) {
                munIdnacimientoNew = em.getReference(munIdnacimientoNew.getClass(), munIdnacimientoNew.getMunId());
                persona.setMunIdnacimiento(munIdnacimientoNew);
            }
            if (munIdresidenciaNew != null) {
                munIdresidenciaNew = em.getReference(munIdresidenciaNew.getClass(), munIdresidenciaNew.getMunId());
                persona.setMunIdresidencia(munIdresidenciaNew);
            }
            if (tipIdNew != null) {
                tipIdNew = em.getReference(tipIdNew.getClass(), tipIdNew.getTipId());
                persona.setTipId(tipIdNew);
            }
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getUsuId());
                persona.setUsuario(usuarioNew);
            }
            List<Telefono> attachedTelefonoListNew = new ArrayList<Telefono>();
            for (Telefono telefonoListNewTelefonoToAttach : telefonoListNew) {
                telefonoListNewTelefonoToAttach = em.getReference(telefonoListNewTelefonoToAttach.getClass(), telefonoListNewTelefonoToAttach.getTelId());
                attachedTelefonoListNew.add(telefonoListNewTelefonoToAttach);
            }
            telefonoListNew = attachedTelefonoListNew;
            persona.setTelefonoList(telefonoListNew);
            persona = em.merge(persona);
            if (munIdnacimientoOld != null && !munIdnacimientoOld.equals(munIdnacimientoNew)) {
                munIdnacimientoOld.getPersonaList().remove(persona);
                munIdnacimientoOld = em.merge(munIdnacimientoOld);
            }
            if (munIdnacimientoNew != null && !munIdnacimientoNew.equals(munIdnacimientoOld)) {
                munIdnacimientoNew.getPersonaList().add(persona);
                munIdnacimientoNew = em.merge(munIdnacimientoNew);
            }
            if (munIdresidenciaOld != null && !munIdresidenciaOld.equals(munIdresidenciaNew)) {
                munIdresidenciaOld.getPersonaList().remove(persona);
                munIdresidenciaOld = em.merge(munIdresidenciaOld);
            }
            if (munIdresidenciaNew != null && !munIdresidenciaNew.equals(munIdresidenciaOld)) {
                munIdresidenciaNew.getPersonaList().add(persona);
                munIdresidenciaNew = em.merge(munIdresidenciaNew);
            }
            if (tipIdOld != null && !tipIdOld.equals(tipIdNew)) {
                tipIdOld.getPersonaList().remove(persona);
                tipIdOld = em.merge(tipIdOld);
            }
            if (tipIdNew != null && !tipIdNew.equals(tipIdOld)) {
                tipIdNew.getPersonaList().add(persona);
                tipIdNew = em.merge(tipIdNew);
            }
            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
                Persona oldPerIdOfUsuario = usuarioNew.getPerId();
                if (oldPerIdOfUsuario != null) {
                    oldPerIdOfUsuario.setUsuario(null);
                    oldPerIdOfUsuario = em.merge(oldPerIdOfUsuario);
                }
                usuarioNew.setPerId(persona);
                usuarioNew = em.merge(usuarioNew);
            }
            for (Telefono telefonoListNewTelefono : telefonoListNew) {
                if (!telefonoListOld.contains(telefonoListNewTelefono)) {
                    Persona oldPerIdOfTelefonoListNewTelefono = telefonoListNewTelefono.getPerId();
                    telefonoListNewTelefono.setPerId(persona);
                    telefonoListNewTelefono = em.merge(telefonoListNewTelefono);
                    if (oldPerIdOfTelefonoListNewTelefono != null && !oldPerIdOfTelefonoListNewTelefono.equals(persona)) {
                        oldPerIdOfTelefonoListNewTelefono.getTelefonoList().remove(telefonoListNewTelefono);
                        oldPerIdOfTelefonoListNewTelefono = em.merge(oldPerIdOfTelefonoListNewTelefono);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = persona.getPerId();
                if (findPersona(id) == null) {
                    throw new NonexistentEntityException("The persona with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Persona persona;
            try {
                persona = em.getReference(Persona.class, id);
                persona.getPerId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The persona with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Usuario usuarioOrphanCheck = persona.getUsuario();
            if (usuarioOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Persona (" + persona + ") cannot be destroyed since the Usuario " + usuarioOrphanCheck + " in its usuario field has a non-nullable perId field.");
            }
            List<Telefono> telefonoListOrphanCheck = persona.getTelefonoList();
            for (Telefono telefonoListOrphanCheckTelefono : telefonoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Persona (" + persona + ") cannot be destroyed since the Telefono " + telefonoListOrphanCheckTelefono + " in its telefonoList field has a non-nullable perId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Municipio munIdnacimiento = persona.getMunIdnacimiento();
            if (munIdnacimiento != null) {
                munIdnacimiento.getPersonaList().remove(persona);
                munIdnacimiento = em.merge(munIdnacimiento);
            }
            Municipio munIdresidencia = persona.getMunIdresidencia();
            if (munIdresidencia != null) {
                munIdresidencia.getPersonaList().remove(persona);
                munIdresidencia = em.merge(munIdresidencia);
            }
            TipoIdentificacion tipId = persona.getTipId();
            if (tipId != null) {
                tipId.getPersonaList().remove(persona);
                tipId = em.merge(tipId);
            }
            em.remove(persona);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Persona> findPersonaEntities() {
        return findPersonaEntities(true, -1, -1);
    }

    public List<Persona> findPersonaEntities(int maxResults, int firstResult) {
        return findPersonaEntities(false, maxResults, firstResult);
    }

    private List<Persona> findPersonaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Persona.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Persona findPersona(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Persona.class, id);
        } finally {
            em.close();
        }
    }

    public int getPersonaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Persona> rt = cq.from(Persona.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
