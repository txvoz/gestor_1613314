/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.controllers;

import co.edu.sena.apiejercicios.jpa.controllers.exceptions.NonexistentEntityException;
import co.edu.sena.apiejercicios.jpa.entities.Archivo;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.edu.sena.apiejercicios.jpa.entities.CategoriaArchivo;
import co.edu.sena.apiejercicios.jpa.entities.TipoArchivo;
import co.edu.sena.apiejercicios.jpa.entities.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author USER
 */
public class ArchivoJpaController implements Serializable {

    public ArchivoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Archivo archivo) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CategoriaArchivo catId = archivo.getCatId();
            if (catId != null) {
                catId = em.getReference(catId.getClass(), catId.getCatId());
                archivo.setCatId(catId);
            }
            TipoArchivo tpaId = archivo.getTpaId();
            if (tpaId != null) {
                tpaId = em.getReference(tpaId.getClass(), tpaId.getTpaId());
                archivo.setTpaId(tpaId);
            }
            Usuario usuId = archivo.getUsuId();
            if (usuId != null) {
                usuId = em.getReference(usuId.getClass(), usuId.getUsuId());
                archivo.setUsuId(usuId);
            }
            em.persist(archivo);
            if (catId != null) {
                catId.getArchivoList().add(archivo);
                catId = em.merge(catId);
            }
            if (tpaId != null) {
                tpaId.getArchivoList().add(archivo);
                tpaId = em.merge(tpaId);
            }
            if (usuId != null) {
                usuId.getArchivoList().add(archivo);
                usuId = em.merge(usuId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Archivo archivo) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Archivo persistentArchivo = em.find(Archivo.class, archivo.getArcId());
            CategoriaArchivo catIdOld = persistentArchivo.getCatId();
            CategoriaArchivo catIdNew = archivo.getCatId();
            TipoArchivo tpaIdOld = persistentArchivo.getTpaId();
            TipoArchivo tpaIdNew = archivo.getTpaId();
            Usuario usuIdOld = persistentArchivo.getUsuId();
            Usuario usuIdNew = archivo.getUsuId();
            if (catIdNew != null) {
                catIdNew = em.getReference(catIdNew.getClass(), catIdNew.getCatId());
                archivo.setCatId(catIdNew);
            }
            if (tpaIdNew != null) {
                tpaIdNew = em.getReference(tpaIdNew.getClass(), tpaIdNew.getTpaId());
                archivo.setTpaId(tpaIdNew);
            }
            if (usuIdNew != null) {
                usuIdNew = em.getReference(usuIdNew.getClass(), usuIdNew.getUsuId());
                archivo.setUsuId(usuIdNew);
            }
            archivo = em.merge(archivo);
            if (catIdOld != null && !catIdOld.equals(catIdNew)) {
                catIdOld.getArchivoList().remove(archivo);
                catIdOld = em.merge(catIdOld);
            }
            if (catIdNew != null && !catIdNew.equals(catIdOld)) {
                catIdNew.getArchivoList().add(archivo);
                catIdNew = em.merge(catIdNew);
            }
            if (tpaIdOld != null && !tpaIdOld.equals(tpaIdNew)) {
                tpaIdOld.getArchivoList().remove(archivo);
                tpaIdOld = em.merge(tpaIdOld);
            }
            if (tpaIdNew != null && !tpaIdNew.equals(tpaIdOld)) {
                tpaIdNew.getArchivoList().add(archivo);
                tpaIdNew = em.merge(tpaIdNew);
            }
            if (usuIdOld != null && !usuIdOld.equals(usuIdNew)) {
                usuIdOld.getArchivoList().remove(archivo);
                usuIdOld = em.merge(usuIdOld);
            }
            if (usuIdNew != null && !usuIdNew.equals(usuIdOld)) {
                usuIdNew.getArchivoList().add(archivo);
                usuIdNew = em.merge(usuIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = archivo.getArcId();
                if (findArchivo(id) == null) {
                    throw new NonexistentEntityException("The archivo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Archivo archivo;
            try {
                archivo = em.getReference(Archivo.class, id);
                archivo.getArcId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The archivo with id " + id + " no longer exists.", enfe);
            }
            CategoriaArchivo catId = archivo.getCatId();
            if (catId != null) {
                catId.getArchivoList().remove(archivo);
                catId = em.merge(catId);
            }
            TipoArchivo tpaId = archivo.getTpaId();
            if (tpaId != null) {
                tpaId.getArchivoList().remove(archivo);
                tpaId = em.merge(tpaId);
            }
            Usuario usuId = archivo.getUsuId();
            if (usuId != null) {
                usuId.getArchivoList().remove(archivo);
                usuId = em.merge(usuId);
            }
            em.remove(archivo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Archivo> findArchivoEntities() {
        return findArchivoEntities(true, -1, -1);
    }

    public List<Archivo> findArchivoEntities(int maxResults, int firstResult) {
        return findArchivoEntities(false, maxResults, firstResult);
    }

    private List<Archivo> findArchivoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Archivo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Archivo findArchivo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Archivo.class, id);
        } finally {
            em.close();
        }
    }

    public int getArchivoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Archivo> rt = cq.from(Archivo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
