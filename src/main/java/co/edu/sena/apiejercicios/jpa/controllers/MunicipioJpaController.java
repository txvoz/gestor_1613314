/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.apiejercicios.jpa.controllers;

import co.edu.sena.apiejercicios.jpa.controllers.exceptions.IllegalOrphanException;
import co.edu.sena.apiejercicios.jpa.controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.edu.sena.apiejercicios.jpa.entities.Departamento;
import co.edu.sena.apiejercicios.jpa.entities.Municipio;
import co.edu.sena.apiejercicios.jpa.entities.Persona;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author USER
 */
public class MunicipioJpaController implements Serializable {

    public MunicipioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Municipio municipio) {
        if (municipio.getPersonaList() == null) {
            municipio.setPersonaList(new ArrayList<Persona>());
        }
        if (municipio.getPersonaList1() == null) {
            municipio.setPersonaList1(new ArrayList<Persona>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Departamento depId = municipio.getDepId();
            if (depId != null) {
                depId = em.getReference(depId.getClass(), depId.getDepId());
                municipio.setDepId(depId);
            }
            List<Persona> attachedPersonaList = new ArrayList<Persona>();
            for (Persona personaListPersonaToAttach : municipio.getPersonaList()) {
                personaListPersonaToAttach = em.getReference(personaListPersonaToAttach.getClass(), personaListPersonaToAttach.getPerId());
                attachedPersonaList.add(personaListPersonaToAttach);
            }
            municipio.setPersonaList(attachedPersonaList);
            List<Persona> attachedPersonaList1 = new ArrayList<Persona>();
            for (Persona personaList1PersonaToAttach : municipio.getPersonaList1()) {
                personaList1PersonaToAttach = em.getReference(personaList1PersonaToAttach.getClass(), personaList1PersonaToAttach.getPerId());
                attachedPersonaList1.add(personaList1PersonaToAttach);
            }
            municipio.setPersonaList1(attachedPersonaList1);
            em.persist(municipio);
            if (depId != null) {
                depId.getMunicipioList().add(municipio);
                depId = em.merge(depId);
            }
            for (Persona personaListPersona : municipio.getPersonaList()) {
                Municipio oldMunIdnacimientoOfPersonaListPersona = personaListPersona.getMunIdnacimiento();
                personaListPersona.setMunIdnacimiento(municipio);
                personaListPersona = em.merge(personaListPersona);
                if (oldMunIdnacimientoOfPersonaListPersona != null) {
                    oldMunIdnacimientoOfPersonaListPersona.getPersonaList().remove(personaListPersona);
                    oldMunIdnacimientoOfPersonaListPersona = em.merge(oldMunIdnacimientoOfPersonaListPersona);
                }
            }
            for (Persona personaList1Persona : municipio.getPersonaList1()) {
                Municipio oldMunIdresidenciaOfPersonaList1Persona = personaList1Persona.getMunIdresidencia();
                personaList1Persona.setMunIdresidencia(municipio);
                personaList1Persona = em.merge(personaList1Persona);
                if (oldMunIdresidenciaOfPersonaList1Persona != null) {
                    oldMunIdresidenciaOfPersonaList1Persona.getPersonaList1().remove(personaList1Persona);
                    oldMunIdresidenciaOfPersonaList1Persona = em.merge(oldMunIdresidenciaOfPersonaList1Persona);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Municipio municipio) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio persistentMunicipio = em.find(Municipio.class, municipio.getMunId());
            Departamento depIdOld = persistentMunicipio.getDepId();
            Departamento depIdNew = municipio.getDepId();
            List<Persona> personaListOld = persistentMunicipio.getPersonaList();
            List<Persona> personaListNew = municipio.getPersonaList();
            List<Persona> personaList1Old = persistentMunicipio.getPersonaList1();
            List<Persona> personaList1New = municipio.getPersonaList1();
            List<String> illegalOrphanMessages = null;
            for (Persona personaListOldPersona : personaListOld) {
                if (!personaListNew.contains(personaListOldPersona)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Persona " + personaListOldPersona + " since its munIdnacimiento field is not nullable.");
                }
            }
            for (Persona personaList1OldPersona : personaList1Old) {
                if (!personaList1New.contains(personaList1OldPersona)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Persona " + personaList1OldPersona + " since its munIdresidencia field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (depIdNew != null) {
                depIdNew = em.getReference(depIdNew.getClass(), depIdNew.getDepId());
                municipio.setDepId(depIdNew);
            }
            List<Persona> attachedPersonaListNew = new ArrayList<Persona>();
            for (Persona personaListNewPersonaToAttach : personaListNew) {
                personaListNewPersonaToAttach = em.getReference(personaListNewPersonaToAttach.getClass(), personaListNewPersonaToAttach.getPerId());
                attachedPersonaListNew.add(personaListNewPersonaToAttach);
            }
            personaListNew = attachedPersonaListNew;
            municipio.setPersonaList(personaListNew);
            List<Persona> attachedPersonaList1New = new ArrayList<Persona>();
            for (Persona personaList1NewPersonaToAttach : personaList1New) {
                personaList1NewPersonaToAttach = em.getReference(personaList1NewPersonaToAttach.getClass(), personaList1NewPersonaToAttach.getPerId());
                attachedPersonaList1New.add(personaList1NewPersonaToAttach);
            }
            personaList1New = attachedPersonaList1New;
            municipio.setPersonaList1(personaList1New);
            municipio = em.merge(municipio);
            if (depIdOld != null && !depIdOld.equals(depIdNew)) {
                depIdOld.getMunicipioList().remove(municipio);
                depIdOld = em.merge(depIdOld);
            }
            if (depIdNew != null && !depIdNew.equals(depIdOld)) {
                depIdNew.getMunicipioList().add(municipio);
                depIdNew = em.merge(depIdNew);
            }
            for (Persona personaListNewPersona : personaListNew) {
                if (!personaListOld.contains(personaListNewPersona)) {
                    Municipio oldMunIdnacimientoOfPersonaListNewPersona = personaListNewPersona.getMunIdnacimiento();
                    personaListNewPersona.setMunIdnacimiento(municipio);
                    personaListNewPersona = em.merge(personaListNewPersona);
                    if (oldMunIdnacimientoOfPersonaListNewPersona != null && !oldMunIdnacimientoOfPersonaListNewPersona.equals(municipio)) {
                        oldMunIdnacimientoOfPersonaListNewPersona.getPersonaList().remove(personaListNewPersona);
                        oldMunIdnacimientoOfPersonaListNewPersona = em.merge(oldMunIdnacimientoOfPersonaListNewPersona);
                    }
                }
            }
            for (Persona personaList1NewPersona : personaList1New) {
                if (!personaList1Old.contains(personaList1NewPersona)) {
                    Municipio oldMunIdresidenciaOfPersonaList1NewPersona = personaList1NewPersona.getMunIdresidencia();
                    personaList1NewPersona.setMunIdresidencia(municipio);
                    personaList1NewPersona = em.merge(personaList1NewPersona);
                    if (oldMunIdresidenciaOfPersonaList1NewPersona != null && !oldMunIdresidenciaOfPersonaList1NewPersona.equals(municipio)) {
                        oldMunIdresidenciaOfPersonaList1NewPersona.getPersonaList1().remove(personaList1NewPersona);
                        oldMunIdresidenciaOfPersonaList1NewPersona = em.merge(oldMunIdresidenciaOfPersonaList1NewPersona);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = municipio.getMunId();
                if (findMunicipio(id) == null) {
                    throw new NonexistentEntityException("The municipio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipio municipio;
            try {
                municipio = em.getReference(Municipio.class, id);
                municipio.getMunId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The municipio with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Persona> personaListOrphanCheck = municipio.getPersonaList();
            for (Persona personaListOrphanCheckPersona : personaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Municipio (" + municipio + ") cannot be destroyed since the Persona " + personaListOrphanCheckPersona + " in its personaList field has a non-nullable munIdnacimiento field.");
            }
            List<Persona> personaList1OrphanCheck = municipio.getPersonaList1();
            for (Persona personaList1OrphanCheckPersona : personaList1OrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Municipio (" + municipio + ") cannot be destroyed since the Persona " + personaList1OrphanCheckPersona + " in its personaList1 field has a non-nullable munIdresidencia field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Departamento depId = municipio.getDepId();
            if (depId != null) {
                depId.getMunicipioList().remove(municipio);
                depId = em.merge(depId);
            }
            em.remove(municipio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Municipio> findMunicipioEntities() {
        return findMunicipioEntities(true, -1, -1);
    }

    public List<Municipio> findMunicipioEntities(int maxResults, int firstResult) {
        return findMunicipioEntities(false, maxResults, firstResult);
    }

    private List<Municipio> findMunicipioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Municipio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Municipio findMunicipio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Municipio.class, id);
        } finally {
            em.close();
        }
    }

    public int getMunicipioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Municipio> rt = cq.from(Municipio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
