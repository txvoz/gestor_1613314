-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-09-2019 a las 23:06:21
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_gestion_archivos`
--
CREATE DATABASE IF NOT EXISTS `db_gestion_archivos` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `db_gestion_archivos`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo`
--

CREATE TABLE `archivo` (
  `arcId` int(11) NOT NULL,
  `arcNombre` varchar(45) COLLATE utf8_bin NOT NULL,
  `arcPeso` varchar(20) COLLATE utf8_bin NOT NULL,
  `arcFechaCreacion` date NOT NULL,
  `arcPalabrasClave` text COLLATE utf8_bin NOT NULL,
  `arcEstado` enum('Publico','Privado') COLLATE utf8_bin NOT NULL,
  `arcPath` varchar(60) COLLATE utf8_bin NOT NULL,
  `catId` int(11) NOT NULL,
  `tpaId` int(11) NOT NULL,
  `usuId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_archivo`
--

CREATE TABLE `categoria_archivo` (
  `catId` int(11) NOT NULL,
  `catNombre` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `depId` int(11) NOT NULL,
  `depNombre` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`depId`, `depNombre`) VALUES
(1, 'Cauca'),
(4, 'Huila'),
(3, 'Nariño'),
(7, 'Prueba'),
(6, 'Santander'),
(2, 'Valle del cauca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `munId` int(11) NOT NULL,
  `munNombre` varchar(45) COLLATE utf8_bin NOT NULL,
  `depId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`munId`, `munNombre`, `depId`) VALUES
(4, 'Popayán', 1),
(5, 'Timbio', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `perId` int(11) NOT NULL,
  `perNombre` varchar(45) COLLATE utf8_bin NOT NULL,
  `perApellido` varchar(45) COLLATE utf8_bin NOT NULL,
  `perIdentificacion` varchar(45) COLLATE utf8_bin NOT NULL,
  `perFechaNacimiento` date NOT NULL,
  `perGenero` enum('M','F') COLLATE utf8_bin NOT NULL,
  `munId_nacimiento` int(11) NOT NULL,
  `munId_residencia` int(11) NOT NULL,
  `tipId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `rolId` int(11) NOT NULL,
  `rolNombre` varchar(25) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`rolId`, `rolNombre`) VALUES
(1, 'Administrador'),
(2, 'Usuario'),
(3, 'nuevo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono`
--

CREATE TABLE `telefono` (
  `telId` int(11) NOT NULL,
  `telNumero` varchar(45) COLLATE utf8_bin NOT NULL,
  `perId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_archivo`
--

CREATE TABLE `tipo_archivo` (
  `tpaId` int(11) NOT NULL,
  `tpaNombre` varchar(30) COLLATE utf8_bin NOT NULL,
  `tpaExt` varchar(5) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tipo_archivo`
--

INSERT INTO `tipo_archivo` (`tpaId`, `tpaNombre`, `tpaExt`) VALUES
(1, 'nombre prueba 1', 'mp4_4'),
(4, 'asdasd', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_identificacion`
--

CREATE TABLE `tipo_identificacion` (
  `tipId` int(11) NOT NULL,
  `tipNombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `tipNomenclatura` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tipo_identificacion`
--

INSERT INTO `tipo_identificacion` (`tipId`, `tipNombre`, `tipNomenclatura`) VALUES
(2, 'Tarjeta de identidad', 'T.I.'),
(3, 'Cedula de ciudadania', 'C.C.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usuId` int(11) NOT NULL,
  `usuCorreo` varchar(50) COLLATE utf8_bin NOT NULL,
  `usuPassword` varchar(45) COLLATE utf8_bin NOT NULL,
  `perId` int(11) NOT NULL,
  `rolId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD PRIMARY KEY (`arcId`),
  ADD KEY `catId` (`catId`),
  ADD KEY `tpaId` (`tpaId`),
  ADD KEY `usuId` (`usuId`);

--
-- Indices de la tabla `categoria_archivo`
--
ALTER TABLE `categoria_archivo`
  ADD PRIMARY KEY (`catId`),
  ADD UNIQUE KEY `catNombre` (`catNombre`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`depId`),
  ADD UNIQUE KEY `depNombre` (`depNombre`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`munId`),
  ADD KEY `depId` (`depId`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`perId`),
  ADD UNIQUE KEY `perIdentificacion` (`perIdentificacion`),
  ADD KEY `munId_nacimiento` (`munId_nacimiento`),
  ADD KEY `munId_residencia` (`munId_residencia`),
  ADD KEY `tipId` (`tipId`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`rolId`),
  ADD UNIQUE KEY `rolNombre` (`rolNombre`);

--
-- Indices de la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD PRIMARY KEY (`telId`),
  ADD KEY `perId` (`perId`);

--
-- Indices de la tabla `tipo_archivo`
--
ALTER TABLE `tipo_archivo`
  ADD PRIMARY KEY (`tpaId`),
  ADD UNIQUE KEY `tpaExt` (`tpaExt`);

--
-- Indices de la tabla `tipo_identificacion`
--
ALTER TABLE `tipo_identificacion`
  ADD PRIMARY KEY (`tipId`),
  ADD UNIQUE KEY `tipNombre` (`tipNombre`),
  ADD UNIQUE KEY `tipNomenclatura` (`tipNomenclatura`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuId`),
  ADD UNIQUE KEY `usuCorreo` (`usuCorreo`),
  ADD UNIQUE KEY `perId` (`perId`),
  ADD KEY `rolId` (`rolId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivo`
--
ALTER TABLE `archivo`
  MODIFY `arcId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categoria_archivo`
--
ALTER TABLE `categoria_archivo`
  MODIFY `catId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `depId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `municipio`
--
ALTER TABLE `municipio`
  MODIFY `munId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `perId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `rolId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `telefono`
--
ALTER TABLE `telefono`
  MODIFY `telId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_archivo`
--
ALTER TABLE `tipo_archivo`
  MODIFY `tpaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_identificacion`
--
ALTER TABLE `tipo_identificacion`
  MODIFY `tipId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuId` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD CONSTRAINT `fk_archivo_categoria` FOREIGN KEY (`catId`) REFERENCES `categoria_archivo` (`catId`),
  ADD CONSTRAINT `fk_archivo_tipo_archivo` FOREIGN KEY (`tpaId`) REFERENCES `tipo_archivo` (`tpaId`),
  ADD CONSTRAINT `fk_archivo_usuario` FOREIGN KEY (`usuId`) REFERENCES `usuario` (`usuId`);

--
-- Filtros para la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD CONSTRAINT `fk_municipio_departamento` FOREIGN KEY (`depId`) REFERENCES `departamento` (`depId`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `fk_persona_municipio_nacimiento` FOREIGN KEY (`munId_nacimiento`) REFERENCES `municipio` (`munId`),
  ADD CONSTRAINT `fk_persona_municipio_residencia` FOREIGN KEY (`munId_residencia`) REFERENCES `municipio` (`munId`),
  ADD CONSTRAINT `fk_persona_tipo_identificacion` FOREIGN KEY (`tipId`) REFERENCES `tipo_identificacion` (`tipId`);

--
-- Filtros para la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD CONSTRAINT `fk_telefono_persona` FOREIGN KEY (`perId`) REFERENCES `persona` (`perId`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_persona` FOREIGN KEY (`perId`) REFERENCES `persona` (`perId`),
  ADD CONSTRAINT `fk_usuario_rol` FOREIGN KEY (`rolId`) REFERENCES `rol` (`rolId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
